;;; Copyright © 2020 Ludovic Courtès <ludo@gnu.org>
;;;
;;; This program is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; This file contains code to deploy and run the libchop algorithm
;; evaluation.  Run with:
;;
;;     guix build -f guix.scm

(use-modules (guix) (gnu)
             (guix git-download)
             (gnu packages gtk)
             (gnu packages guile)
             (gnu packages guile-xyz)
             (gnu packages plotutils)
             (past packages backup)
             (srfi srfi-1)
             (ice-9 match))


;;;
;;; File sets.
;;;

(define file-set/gnu-manuals
  ;; The "gnu-manuals" file set.
  (let ((commit "9572cc2870e597f6323cc544d85929b3f14fb4bb"))
    (origin
      (method git-fetch)
      (uri (git-reference
            (url "https://gitlab.inria.fr/lcourtes-phd/file-sets.git")
            (commit commit)))
      (file-name (string-append "file-sets-" (string-take commit 7)))
      (sha256
       (base32
        "1mq83dzh6r27yrmb1qsclgzxk9by7pkqd1vwkhy4r02gfxzb064y")))))

(define (lout-tarball file hash)
  (origin
    (method url-fetch)
    (uri (string-append "mirror://savannah/lout/" file))
    (sha256 (base32 hash))))

(define file-set/lout-distributions
  ;; The "lout-distributions" file set.
  (map (match-lambda
         ((file sha256)
          (lout-tarball file sha256)))
       '(("lout-3.20.tar.gz"
          "0swj8jk2l3pkw1ryrlscgpbpxz0p0bx544cz0hkx845lp18bhqmg")
         ("lout-3.21.tar.gz"
          "1kmp164i33kj0h1vng44bdwqqgqc9lw70qlnrz8mpks7yz3ng109")
         ("lout-3.22.tar.gz"
          "1hpzqxl3nxbk1smvlij2dzkks3qw45y76kbcjin40xwpzwf97g6q")
         ("lout-3.23.tar.gz"
          "0fpxyb3d60cyybjf0h1p2nhk4g71sg7l4a9jmadakdjs0qwqm227")
         ("lout-3.24.tar.gz"
          "15fj8ylai86k0a8ds9scrn9py5zbbm21ibjljz7z84jbq7cgnhs9")
         ("lout-3.25.tar.gz"
          "1zz28pnp75y2dsn7q8zg4mshfivk7263zg0a84iv8vqm5x8c7ihn")
         ("lout-3.26.tar.gz"
          "1yaq47ysy0rjjabj1g64v2x73gk7hc7lx45byfisgc3zhvkn4fxa")
         ("lout-3.27.tar.gz"
          "0cfr5b35834hrrf57l71hzhxdb2lxx4g1w761za6s5fgk5p3zqmj")
         ("lout-3.28.tar.gz"
          "1yia1kyhjc271nb67c9cbcsplgdvjwbcs2zbg43q6zy3zbnpwvfg")
         ("lout-3.29.tar.gz"
          "0pzplr76wc4rvspv2z8ms1k2khwnfvrm6lcivwrshjw1hdf4hhg9"))))

(define (vorbis-file url hash)
  (origin
    (method url-fetch)
    (uri url)
    (sha256 (base32 hash))))

(define vorbis-files
  (map (match-lambda
         ((url sha256)
          (vorbis-file url sha256)))
       '(("https://archive.org/download/nine_inch_nails_the_slip/01_999999.ogg"
          "1hdi917g88rywlzarl0qsi4h4b3kv3akx5s5405gsi7q2yjhsfzf")
         ("https://archive.org/download/nine_inch_nails_the_slip/02_1000000.ogg"
          "1j1qk3fsc5pd2098jalzjhcj3ylgw4bwnlk3bsppf01gww02wf39")
         ("https://archive.org/download/nine_inch_nails_the_slip/03_letting_you.ogg"
          "11fc9hlrzg504kamlcyzz0r66c0grf02yzx9di0dh7bnpq3n4jdi")
         ("https://archive.org/download/nine_inch_nails_the_slip/04_discipline.ogg"
          "0x0gx7aszfaiq038j5k7d0zp413vzmlsn2pr1mn8jridq5sh2wj7")
         ("https://archive.org/download/nine_inch_nails_the_slip/05_echoplex.ogg"
          "0hih1aaql91mndpgfj1qk4bpyzykwd6kynprrd88w4bmfmsdcjqi")
         ("https://archive.org/download/nine_inch_nails_the_slip/06_head_down.ogg"
          "0db42z8j98k9xxfsvrac1g1dqiw51vk4zknx8a4691qs35jm31w7")
         ("https://archive.org/download/nine_inch_nails_the_slip/07_lights_in_the_sky.ogg"
          "07dglqz1slzazgvax59a8bqmq6xrsb436vrbmagkjrwpg293cqwy")
         ("https://archive.org/download/nine_inch_nails_the_slip/08_corona_radiata.ogg"
          "00yb3pywf5qzrm2349b1nv0sln4fdshd1w69gkyzklb7wyvkxhbw")
         ("https://archive.org/download/nine_inch_nails_the_slip/09_the_four_of_us_are_dying.ogg"
          "1vy84ma4q5shjw7qxr7ngpza7w674wampamb5qw5ipnnhl49f9qx")
         ("https://archive.org/download/nine_inch_nails_the_slip/10_demon_seed.ogg"
          "17na7z7byr3qzyjdvdqkh2g324mbq5pk15455drlpq4fx8pf8nqv"))))

(define file-set/vorbis
  ;; The "ogg-vorbis" file set.
  (computed-file "vorbis"
                 (with-imported-modules '((guix build utils))
                   #~(begin
                       (use-modules (guix build utils))
                       (for-each (lambda (file)
                                   (install-file file #$output))
                                 '#$vorbis-files)))))

(define file-set/mbox
  ;; The "mbox" file set (7.5 MiB).
  (origin
    (method url-fetch)
    (uri "https://lists.gnu.org/archive/mbox/guix-devel/2016-04")
    (file-name "mbox.txt")
    (sha256
     (base32
      "0r4c9firbxk735avvdyxighjn55bq4ci0g4p7raspcigsnryw10y"))))

(define file-set-cache
  ;; Cache for use as ~/.chop-eval/cache/web.
  (computed-file "file-set-cache"
                 (with-imported-modules '((guix build utils))
                   #~(begin
                       (use-modules (guix build utils))

                       (for-each (lambda (file)
                                   (install-file file #$output))
                                 (find-files #$file-set/gnu-manuals
                                             "\\.ps.gz"))
                       (for-each (lambda (file)
                                   (let* ((base   (strip-store-file-name file))
                                          (cached (string-append #$output "/"
                                                                 base)))
                                     (copy-file file cached)))
                                 '#$file-set/lout-distributions)))))


;;;
;;; Evaluation.
;;;

(define chop-eval
  ;; Scripts to perform the evaluation shown in the paper.
  (let ((commit "b8aac089e164c39bcc5b8cd97b96c5aaabf597ba")) ;13 Apr. 2006
    (origin
      (method git-fetch)
      (uri (git-reference
            (url "https://gitlab.inria.fr/lcourtes-phd/chop-eval.git")
            (commit commit)))
      (file-name (string-append "chop-eval-" (string-take commit 7)))
      (sha256
       (base32
        "0ildad6b9m58nn8g7rjrss1snfc972fjkagbsamy36d1wkpmybad")))))

(define libchop&co
  ;; Libchop and its propagated inputs
  (append-map (lambda (package)
                (cons package
                      (match (package-transitive-propagated-inputs package)
                        (((labels packages) ...)
                         packages))))
              (list libchop/guile-1.8)))

(define evaluation
  ;; The actual evaluation.
  (let ()
    (define build
      (with-imported-modules '((guix build utils))
        #~(begin
            (use-modules (guix build utils))

            (setenv "GUILE_LOAD_PATH"
                    (string-join '(#$@libchop&co) "/share/guile/site:"
                                 'suffix))
            (setenv "PATH"
                    (string-join (list #$guile-1.8
                                       #$(specification->package "coreutils")
                                       #$(specification->package "bash")
                                       #$(specification->package "gzip")
                                       #$(specification->package "tar")

                                       ;; Fun fact: if 'system*' fails to
                                       ;; execv(2), the child process keeps
                                       ;; going instead of exiting!  Thus,
                                       ;; provide 'wget' even if it won't be
                                       ;; able to do anything.
                                       #$(specification->package "wget"))
                                 "/bin:" 'suffix))

            ;; Provide a valid shebang for the scripts.
            (copy-recursively #$chop-eval ".")
            (for-each patch-shebang
                      '("run.sh" "chop-eval.scm"))

            ;; Allow the local Guile modules to be found.
            (setenv "GUILE_LOAD_PATH"
                    (string-append (getcwd) "/module:"
                                   (getenv "GUILE_LOAD_PATH")))
            (mkdir #$output)

            ;; Make the file sets available in the local "cache".  It must be
            ;; copied because (chop utils cache) attempts to link(2) to it.
            (setenv "HOME" (getcwd))
            (copy-recursively #$file-set-cache
                              ".chop-eval/cache/web")

            ;; Make the mbox file set available under the name "mailbox", so
            ;; we can more easily refer to it.
            (symlink #$file-set/mbox "mailbox")

            (substitute* "run.sh"
              (("^file_sets=.*")
               (string-append "file_sets=\"built-in:gnu-manuals-postscript \
built-in:lout-distributions ogg-vorbis mailbox\"\n"))
              (("fs=\".*\\.ogg\"")
               (string-append "fs=\"" #$file-set/vorbis "/*.ogg\"")))

            ;; Capture stdout, which contains benchmarking results, for
            ;; post-processing.
            (with-output-to-file (string-append #$output "/stdout")
              (lambda ()
                (invoke "./run.sh"))))))
    (computed-file "evaluation" build)))

(define benchmark-results
  ;; Performance data, as an association list ("alist").
  (computed-file "benchmark-results.txt"
                 (with-imported-modules '((parse-log))
                   #~(begin
                       (use-modules (parse-log)
                                    (ice-9 pretty-print))

                       (define result
                         (call-with-input-file
                             #$(file-append evaluation "/stdout")
                           parse-log))

                       (call-with-output-file #$output
                         (lambda (port)
                           (display ";; Benchmarking results.\n\n" port)
                           (pretty-print result port)))))))

(define benchmark-charts
  ;; PDF charts showing the throughput and size for each configuration and
  ;; file set.
  (computed-file "benchmark-charts"
                 (with-extensions (list guile-cairo guile-charting)
                   (with-imported-modules '((plot)
                                            (guix build utils))
                     #~(begin
                         (use-modules (plot)
                                      (guix build utils))

                         (let ((data (call-with-input-file #$benchmark-results
                                       read)))
                           (make-throughput-plot data "throughput.pdf")
                           (make-size-plot data "size.pdf")
                           (install-file "throughput.pdf" #$output)
                           (install-file "size.pdf" #$output)))))))

;; Add "." to the load path so that 'parse-log.scm' is found.
(add-to-load-path ".")

benchmark-charts
