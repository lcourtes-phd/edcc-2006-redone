;;; Copyright © 2020 Ludovic Courtès <ludo@gnu.org>
;;;
;;; This program is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (parse-log)
  #:use-module (ice-9 regex)
  #:use-module (ice-9 rdelim)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:export (parse-log))

;;; Commentary:
;;;
;;; This module provides a procedure to parse the output of 'chop-eval'.
;;;
;;; Code:

(define (all-matches match)
  "Return all the parenthetical matches in MATCH."
  (let ((count (match:count match)))
    (unfold (cut >= <> count)
            (cut match:substring match <>)
            1+
            1)))

(define-syntax parse-line
  (syntax-rules (else)
    "Return a procedure that parses one line according to the given rules."
    ((_ ((regexp bindings ...) exp) rest ... (else fallback))
     (lambda (line)
       (let* ((next   (parse-line rest ... (else fallback)))
              (result (string-match regexp line)))
         (if result
             (call-with-values (lambda ()
                                 (apply values (all-matches result)))
               (lambda (bindings ...)
                 exp))
             (next line)))))
    ((_ (else fallback)) (const fallback))))

(define (parsing-loop seed line-proc)
  "Return a procedure that reads from the given port line by line, calling
LINE-PROC on each line, and returning the result."
  (lambda (port)
    (let loop ((line (read-line port))
               (result seed))
      (if (eof-object? line)
          result
          (loop (read-line port)
                (line-proc line result))))))

(define-syntax-rule (parse/fold state seed rules ...)
  (let ((line (lambda (line state)
                ((parse-line rules ...) line))))
    (parsing-loop seed line)))

(define parse-log
  ;; Parse the output of 'chop-eval'.  Return a list of alists, where each
  ;; alist contains data for one run.
  (let ((put (lambda (result key value)
               ;; Add the KEY/VALUE pair to the first alist of RESULT.
               (match result
                 ((first rest ...)
                  `(((,key . ,value) ,@first) ,@rest)))))
        (current-zip #f))
    (parse/fold result '()
                (("^\\*\\*\\* c=.* z=([[:graph:]]*)$" zip)
                 ;; XXX: The input/block compression method is not printed
                 ;; for every run, hence this ugly hack.
                 (begin
                   (set! current-zip
                     (match zip
                       ("" #f)
                       ("--zip-blocks" 'blocks)
                       ("--zip-input" 'input)))
                   result))
                (("^file set: ([[:graph:]]+)" file-set)
                 ;; This is the first line of a run: add a new alist for this
                 ;; run to RESULT.
                 (cons `((file-set . ,file-set) (zip . ,current-zip))
                       result))
                (("^  chopper class: ([[:graph:]]+)" chopper)
                 (put result 'chopper chopper))
                (("^  block indexer class: ([[:graph:]]+)" indexer)
                 (put result 'block-indexer indexer))
                (("^  requested block size: ([[:digit:]]+)" number)
                 (put result 'block-size (string->number number)))
                (("^  input size: ([[:digit:]]+)" number)
                 (put result 'input-size (string->number number)))
                (("^  actual data size.*: ([[:digit:]]+) bytes" number)
                 (put result 'size (string->number number)))
                (("^  throughput: +([0-9.]+)" number)
                 (put result 'throughput (string->number number)))
                ;; Ignore lines that don't match.
                (else result))))
