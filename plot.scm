;;; Copyright © 2020 Ludovic Courtès <ludo@gnu.org>
;;;
;;; This program is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (plot)
  #:use-module (cairo)
  #:use-module (charting)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:export (make-throughput-plot
            make-size-plot))

;;; Commentary:
;;;
;;; This module provides procedures to render performance charts as PDF.
;;;
;;; Code:

;; XXX: Work around a bug in Guile-Charting 0.2.0 whereby the procedure
;; returned by 'text-measurer' can return an inexact number, whereas callers
;; expect an exact number.
(set! (@@ (charting) text-measurer)
  (let ((measurer (@@ (charting) text-measurer)))
    (lambda* (#:optional (family "Bitstream Vera Sans") (size 10))
      (let ((proc (measurer family size)))
        (lambda (text)
          (inexact->exact (round (proc text))))))))

(define-syntax alist-let
  (syntax-rules ()
    "Bind the given KEYs in EXP to the corresponding items in ALIST.  ALIST
is assumed to be a list of two-element tuples rather than a traditional list
of pairs."
    ((_ alist ((var key) ...) exp ...)
     (let ((var (assoc-ref alist 'key)) ...)
       exp ...))))

(define* (select-run data #:key file-set chopper block-indexer block-size
                     (zip *unspecified*))
  "Select from DATA the run that matches the given arguments."
  (any (lambda (run)
         (alist-let run ((file-set* file-set)
                         (chopper* chopper)
                         (block-size* block-size)
                         (block-indexer* block-indexer)
                         (zip* zip))
           (and (string=? file-set* file-set)
                chopper*
                (string=? chopper* chopper)
                (or (not block-size)
                    (= block-size* block-size))
                (or (not block-indexer)
                    (string=? block-indexer* block-indexer))
                (or (eq? zip *unspecified*)
                    (equal? zip zip*))
                run)))
       data))

(define configurations
  ;; See Figure 4 of the paper for a description of each configuration.
  '(("A1"
     . (#:chopper "whole_stream" #:block-indexer "uuid" #:zip input
        #:block-size 1024))                       ;shouldn't matter
    ("A2"
     . (#:chopper "whole_stream" #:block-indexer "hash" #:zip input
        #:block-size 1024))
    ("B1"
     . (#:chopper "anchor_based" #:block-indexer "hash" #:zip #f
        #:block-size 1024))
    ("B2"
     . (#:chopper "anchor_based" #:block-indexer "hash" #:zip blocks
        #:block-size 1024))
    ("B3"
     . (#:chopper "fixed_size" #:block-indexer "hash" #:zip blocks
        #:block-size 1024))
    ("C"
     . (#:chopper "fixed_size" #:block-indexer "hash" #:zip input
        #:block-size 1024))))

(define (data->performance-series data extract-value)
  (map (lambda (pretty-name file-set)
         `(,pretty-name
           ,@(filter-map (match-lambda
                           ((name . arguments)
                            (let ((run (apply select-run data
                                              #:file-set file-set
                                              arguments)))
                              (and run `(,name ,(extract-value run))))))
                         configurations)))
       '("Lout" "Ogg Vorbis" "mailbox")
       '("built-in:lout-distributions" "ogg-vorbis" "mailbox")))

(define (pdf-surface file)
  (lambda (x y)
    (cairo-pdf-surface-create x y file)))

(define (make-throughput-plot data file)
  "Write to FILE a PDF containing a chart of the throughput extracted from
DATA.  DATA is an alist as returned by by 'parse-log'."
  (define surface
    (make-performance-chart #:title ""
                            #:box-width 20
                            #:data (data->performance-series
                                    data (lambda (run)
                                           (/ (assoc-ref run 'throughput)
                                              1024.)))
                            #:chart-height 400
                            #:chart-params
                            `(#:x-axis-label "Configuration"
                              #:y-axis-label "Throughput (KiB/s)"
                              #:make-surface ,(pdf-surface file))))

  (cairo-surface-finish surface)
  surface)

(define (make-size-plot data file)
  "Write to FILE a PDF containing a chart of the size extracted from DATA.
DATA is an alist as returned by by 'parse-log'."
  (define surface
    (make-performance-chart #:title ""
                            #:box-width 20
                            #:data (data->performance-series
                                    data
                                    (lambda (run)
                                      (alist-let run ((size size)
                                                      (input-size input-size))
                                        (* 100. (/ size input-size)))))
                            #:chart-height 400
                            #:chart-params
                            `(#:x-axis-label "Configuration"
                              #:y-axis-label "Space savings (%)"
                              #:make-surface ,(pdf-surface file))))

  (cairo-surface-finish surface)
  surface)

;;; Local Variables:
;;; eval: (put 'alist-let 'scheme-indent-function 2)
;;; End:
