[_Storage Tradeoffs in a Collaborative Backup Service for Mobile Devices_](https://hal.inria.fr/hal-00187069/en) redone
========================================================================
[![pipeline status](https://gitlab.inria.fr/lcourtes-phd/edcc-2006-redone/badges/master/pipeline.svg)](https://gitlab.inria.fr/lcourtes-phd/edcc-2006-redone/commits/master) [![SWH](https://archive.softwareheritage.org/badge/origin/https://gitlab.inria.fr/lcourtes-phd/edcc-2006-redone/)](https://archive.softwareheritage.org/browse/origin/https://gitlab.inria.fr/lcourtes-phd/edcc-2006-redone/)

This repository contains code to deploy and run the software and file
sets used to perform the evaluations that appear in [_Storage Tradeoffs
in a Collaborative Backup Service for Mobile
Devices_](https://hal.inria.fr/hal-00187069/en), published at the
[European Dependable Computing Conference
(EDCC)](http://edcc.dependability.org/),
[2006](https://dblp.uni-trier.de/db/conf/edcc/edcc2006).

This replication was
[submitted](https://github.com/ReScience/submissions/issues/32) to the
[“Ten Year Reproducibility
Challenge”](https://rescience.github.io/ten-years/) of ReScience C.

To build and run the evaluation, install
[GNU Guix](https://guix.gnu.org) and type:

```
guix time-machine -C channels.scm -- build -f guix.scm
```

# Links

  - [libchop](https://www.nongnu.org/libchop/), the software benchmarked
    in the article
  - [chop-eval](https://gitlab.inria.fr/lcourtes-phd/chop-eval/-/tree/lcourtes@laas.fr--2004-mosaic,chop-eval--devo--0.1),
    the libchop benchmarking tools
  - [source code of the original
    article](https://gitlab.inria.fr/lcourtes-phd/edcc-2006) (typeset
    with [Skribilo](https://nongnu.org/skribilo/) and
    [Lout](https://savannah.nongnu.org/projects/lout))
