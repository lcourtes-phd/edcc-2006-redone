;;; Copyright © 2020 Ludovic Courtès <ludo@gnu.org>
;;;
;;; This program is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; This file contains code to build the ReScience C article.  Run with:
;;
;;     guix build -f guix.scm

(use-modules (guix) (gnu)
             (guix graph)
             ((guix scripts graph) #:select (%package-node-type))
             (guix git-download)
             (git))

(define locales
  (specification->package "glibc-utf8-locales"))

(define python
  (specification->package "python"))
(define python-pyyaml
  (specification->package "python-pyyaml"))
(define python-dateutil
  (specification->package "python-dateutil"))
(define python-six
  (specification->package "python-six"))
(define biber
  (specification->package "biber"))
(define texlive
  (specification->package "texlive"))

(define texlive
  ;; FIXME: Use the modular texlive along these lines:
  ;; (texlive-union (map specification->package
  ;;                     '("texlive-base" "texlive-beamer"
  ;;                       "texlive-fonts-ec" "texlive-etoolbox"
  ;;                       "texlive-generic-ulem" "texlive-cm-super" "texlive-latex-capt-of"
  ;;                       "texlive-latex-wrapfig" "texlive-latex-geometry"
  ;;                       "texlive-latex-ms" "texlive-amsfonts" "texlive-latex-graphics"
  ;;                       "texlive-latex-pgf"
  ;;                       "texlive-marginnote" "texlive-csquotes")))
  (specification->package "texlive"))

(define rescience-template
  (origin
    (method git-fetch)
    (uri (git-reference (url "https://github.com/rescience/template")
                        (commit "93ead8f348925aa2c649e2a55c6e16e8f3ab64a5")))
    (file-name "rescience-template")
    (sha256
     (base32 "10xrflbkrv6bq92nd169y5jpsv36dk4i6h765026wln7kpyfwk8j"))))

(define metadata.tex
  (computed-file "metadata.tex"
                 #~(begin
                     (setenv "GUIX_LOCPATH"
                             #$(file-append locales "/lib/locale"))
                     (setenv "LC_ALL" "en_US.utf8")
                     (copy-file #$(local-file "article.py") "article.py")
                     (setenv "PYTHONPATH"
                             (let ((suffix
                                    #$(string-append "/lib/python"
                                                     (version-major+minor
                                                      (package-version python))
                                                     "/site-packages:")))
                               (string-append
                                (getcwd) ":"
                                (string-join '(#$python-pyyaml
                                               #$python-dateutil
                                               #$python-six)
                                             suffix 'suffix))))

                     (zero? (system* #$(file-append python "/bin/python3")
                                     #$(local-file "yaml-to-latex.py")
                                     "-i" #$(local-file "metadata.yaml")
                                     "-o" #$output)))))

(define source
  (local-file "." "rescience-c-article-source"
              #:recursive? #t
              #:select? (lambda (file stat)
                          (or (string-suffix? ".tex" file)
                              (string-suffix? ".bib" file)
                              (string-suffix? ".cls" file)))))

(define libchop-graph.dot
  ;; Package dependency graph of libchop.
  (plain-file "libchop-graph.dot"
              (call-with-output-string
                (lambda (port)
                  (run-with-store #f
                    (export-graph (list (specification->package "libchop@0.0"))
                                  port
                                  #:node-type %package-node-type))))))

(define libchop-graph.pdf
  ;; PDF of the dependency graph.
  (computed-file "libchop-graph.pdf"
                 #~(system* #$(file-append (specification->package "graphviz")
                                           "/bin/dot")
                            "-Tpdf" "-Gratio=.7" "-Gnodesep=.05"
                            #$libchop-graph.dot "-o" #$output)))


;; Include the top-level file, which provides charts.
(include "../guix.scm")

(define (this-commit)
  "Return the commit ID for the tip of this repository."
  (let* ((repository (repository-open "."))
         (head       (repository-head repository))
         (target     (reference-target head))
         (id         (oid->string target)))
    (repository-close! repository)
    id))

(define article.pdf
  ;; Building the PDF.  Adapted from the ReScience template makefile.
  (let ()
    (define build
      (with-imported-modules '((guix build utils))
        #~(begin
            (use-modules (guix build utils))

            (copy-recursively #$source ".")
            (copy-file #$metadata.tex "metadata.tex")

            (for-each (lambda (font)
                        (symlink (string-append #$rescience-template "/"
                                                font)
                                 font))
                      '("roboto" "source-code-pro"
                        "source-sans-pro" "source-serif-pro"))

            (setenv "PATH"
                    (string-join '(#$texlive #$biber)
                                 "/bin:" 'suffix))

            ;; Provide the charts (PDF files) in the chart/ sub-directory.
            (symlink #$benchmark-charts "charts")

            ;; Likewise for the dependency graph.
            (symlink #$libchop-graph.pdf "libchop-graph.pdf")

            ;; Store the current commit ID.
            (call-with-output-file "commit.tex"
              (lambda (port)
                (display #$(this-commit) port)))

            (invoke "latexmk" "-pdf"
                    "-pdflatex=xelatex -interaction=nonstopmode"
                    "-use-make" "article.tex")
            (copy-file "article.pdf" #$output))))

    (computed-file "article.pdf" build)))

article.pdf
