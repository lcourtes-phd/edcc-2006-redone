;; This file lists channels that GNU Guix should pull in order to be able to
;; deploy all the software 'guix.scm' refers to.

(list (channel
       (name 'guix-past)
       (url "https://gitlab.inria.fr/guix-hpc/guix-past.git")
       (commit "4c3923dc0114f4669fbd99c5a09a443d3eb5f4d6"))
      (channel
       (name 'guix)
       (url "https://git.savannah.gnu.org/git/guix.git")
       (commit "40fd909e3ddee2c46a27a4fe92ed49d3e7ffb413")))
